<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('code')->unique();
	        $table->integer('category_id')->unsigned();
	        $table->integer('subcategory_id')->unsigned();
	        $table->integer('lesson_name_id')->unsigned();
	        $table->text('content');
	        $table->integer('preread_id')->unsigned()->nullable();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lessons');
    }
}
