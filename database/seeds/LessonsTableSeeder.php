<?php

use Illuminate\Database\Seeder;

class LessonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('lessons')->insert([
	    	[
	    		'code' => 'Y0001',
			    'category_id' => 1,
			    'subcategory_id' => 2,
			    'lesson_name_id' => 1,
			    'content' => 'Bài số 1',
			    'preread_id' => null,
		    ],
		    [
			    'code' => 'Y0002',
			    'category_id' => 2,
			    'subcategory_id' => 1,
			    'lesson_name_id' => 2,
			    'content' => 'Bài số 2',
			    'preread_id' => 1,
		    ],
		    [
			    'code' => 'Y0003',
			    'category_id' => 3,
			    'subcategory_id' => 1,
			    'lesson_name_id' => 3,
			    'content' => 'Bài số 3',
			    'preread_id' => 1,
		    ],
		    [
			    'code' => 'Y0004',
			    'category_id' => 2,
			    'subcategory_id' => 4,
			    'lesson_name_id' => 5,
			    'content' => 'Bài số 4',
			    'preread_id' => 2,
		    ],
		    [
			    'code' => 'Y0005',
			    'category_id' => 3,
			    'subcategory_id' => 1,
			    'lesson_name_id' => 5,
			    'content' => 'Bài số 5',
			    'preread_id' => 3,
		    ],
		    [
			    'code' => 'Y0006',
			    'category_id' => 5,
			    'subcategory_id' => 4,
			    'lesson_name_id' => 6,
			    'content' => 'Bài số 6',
			    'preread_id' => 2,
		    ],
		    [
			    'code' => 'Y0007',
			    'category_id' => 1,
			    'subcategory_id' => 1,
			    'lesson_name_id' => 7,
			    'content' => 'Bài số 7',
			    'preread_id' => 1,
		    ],
	    ]);
    }
}
