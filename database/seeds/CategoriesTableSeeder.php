<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('categories')->insert([
		    [
			    'name' => 'A1.1',
		    ],
		    [
			    'name' => 'A1.2'
		    ],
		    [
			    'name' => 'A1.3'
		    ],
		    [
			    'name' => 'B1.1'
		    ],
		    [
			    'name' => 'B1.2'
		    ],
		    [
			    'name' => 'B1.3'
		    ],
	    ]);
    }
}
