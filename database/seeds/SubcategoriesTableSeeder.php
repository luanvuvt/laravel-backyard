<?php

use Illuminate\Database\Seeder;

class SubcategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('subcategories')->insert([
		    [
			    'name' => 'Part1',
		    ],
		    [
			    'name' => 'Part2'
		    ],
		    [
			    'name' => 'Part3'
		    ],
		    [
			    'name' => 'Part4'
		    ],
	    ]);
    }
}
