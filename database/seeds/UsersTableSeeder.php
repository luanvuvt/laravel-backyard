<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('users')->insert([
		    'name' => 'Luan Vu',
		    'email' => 'luanvvt@lifetimetech.vn',
		    'password' => bcrypt('123456'),
	    ]);
    }
}
