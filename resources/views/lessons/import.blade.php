@extends('layouts.app')

@section('title', 'CSV Import')

@section('content')
    <form id="dropzone" class="form-horizontal form-main dropzone" method="post" action="{{ url('/lesson/upload-csv') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
    </form>

    <div class="dropzone-previews"></div>

    <form class="form-horizontal form-import" method="post" action="{{ url('/lesson/do-import') }}" style="display: none; margin-top: 20px; min-height: 100px;">
        {{ csrf_field() }}
        <input type="hidden" name="file_name" value="" />
        <div class="form-group btn-group-import">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Đăng ký</button>
            </div>
        </div>
    </form>

    <script type="text/template" class="template-result">
        <div class="dz-preview dz-file-preview">
            <div class="dz-details">
                <div class="dz-filename"><span data-dz-name></span></div> -
                <div class="dz-size" data-dz-size></div>
            </div>
            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
            <div class="dz-error-message"><span data-dz-errormessage></span></div>
        </div>
    </script>
@endsection

@push('scripts')
    <script src="{{ url('js/dropzone.js') }}"></script>
@endpush
