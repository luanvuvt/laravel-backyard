@extends('layouts.app')

@section('title', 'Quản lý bài học')

@section('content')
    <ul class="nav nav-pills sub-navigation">
        <li role="presentation"><a href="{{ url('/lesson/create') }}">Đăng kí mới</a></li>
        <li role="presentation"><a href="{{ url('/lesson/import-csv') }}">Nhập qua CSV</a></li>
    </ul>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Điều kiện tìm kiếm</h3>
        </div>
        <form class="form-horizontal form-search-lesson" method="get" action="{{ url('/lesson') }}">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-9">
                            <div class="form-group">
                                <label for="search-category" class="col-sm-2 control-label">Mục trung</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="search-category" name="category_id">
                                        <option value="0">Tất cả</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}" @if ($criteria['category_id'] == $category->id) selected="selected" @endif>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="search-subcategory" class="col-sm-2 control-label">Mục nhỏ</label>
                                <div class="col-sm-10">
                                    @foreach ($subcategories as $subcategory)
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="subcategory_id[]" value="{{ $subcategory->id }}" {{ in_array($subcategory->id, $criteria['subcategory_id']) ? 'checked="checked"' : '' }}> {{ $subcategory->name }}
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="search-subcategory" class="col-sm-2 control-label">Đọc trước</label>
                                <div class="col-sm-10">
                                    <label class="radio-inline">
                                        <input type="radio" name="preread" value="1" {{ 1 == $criteria['preread'] ? 'checked="checked"' : '' }}> Có
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="preread" value="0" {{ 0 == $criteria['preread'] ? 'checked="checked"' : '' }}> Không
                                    </label>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-primary btn-search">Tìm kiếm</button>
                        <a href="{{ url('/lesson') }}" class="btn btn-default btn-clear">Xóa lựa chọn</a>
                    </div>
                </div>
            </div>
        </form>
    </div>

    @if ($lessons->count())
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Mục trung</th>
                    <th>Mục nhỏ</th>
                    <th>Bài học</th>
                    <th>Nội dung</th>
                    <th>Đọc trước</th>
                    <th>Bài đọc trước</th>
                    <th>Mã bài học</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($lessons as $lesson)
                    <tr>
                        <td>{{ $lesson->category->name }}</td>
                        <td>{{ $lesson->subcategory->name }}</td>
                        <td>{{ $lesson->lessonName->name }}</td>
                        <td>{{ $lesson->content }}</td>
                        <td>{{ $lesson->is_preread ? 'Có' : 'Không' }}</td>
                        <td>{{ $lesson->preread ? $lesson->preread->code : '-' }}</td>
                        <td><a href="{{ url('/lesson/' . $lesson->id . '/edit') }}">{{ $lesson->code }}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div style="margin-bottom: 20px;">Không có bài học nào thỏa mãn điều kiện tìm kiếm.</div>
    @endif

    {{ $lessons->appends($criteria)->links() }}
@endsection
