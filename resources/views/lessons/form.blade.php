@extends('layouts.app')

@section('title', 'Đăng ký bài học mới')

@section('content')
    <form class="form-horizontal form-main form-lesson" method="post" action="{{ $creating ? url('/lesson') : url('/lesson/' . $lesson->id) }}">
        {{ csrf_field() }}

    @unless ($creating)
            {{ method_field('PUT') }}
        @endunless
        <input type="hidden" name="id" value="{{ $lesson->id }}" />
        <div class="form-group">
            <label for="input-category" class="col-sm-2 control-label">Mục trung</label>
            <div class="col-sm-5">
                <select class="form-control" name="category_id" id="input-category">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}" @if ($lesson->category_id == $category->id) selected="selected" @endif>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="input-subcategory" class="col-sm-2 control-label">Mục nhỏ</label>
            <div class="col-sm-5">
                <select class="form-control" name="subcategory_id" id="input-subcategory">
                    @foreach ($subcategories as $subcategory)
                        <option value="{{ $subcategory->id }}" @if ($lesson->subcategory_id == $subcategory->id) selected="selected" @endif>{{ $subcategory->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="input-lesson" class="col-sm-2 control-label">Bài học</label>
            <div class="col-sm-5">
                <select class="form-control" name="lesson_name_id" id="input-lesson">
                    @foreach ($lesson_names as $lesson_name)
                        <option value="{{ $lesson_name->id }}" @if ($lesson->lesson_name_id == $lesson_name->id) selected="selected" @endif>{{ $lesson_name->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group {{ $errors->has('preread') ? ' has-error' : '' }}">
            <label class="col-sm-2 control-label">Đọc trước</label>
            <div class="col-sm-10">
                <label class="radio-inline">
                    <input type="radio" id="input-preread-yes" name="preread" value="1" @if (true == $is_preread) checked="checked" @endif> Có
                </label>
                <label class="radio-inline">
                    <input type="radio" id="input-preread-no" name="preread" value="0" @if (false == $is_preread) checked="checked" @endif> Không
                </label>
                @if ($errors->has('preread'))
                    <span class="help-block">
                        <strong>{{ $errors->first('preread') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group {{ $errors->has('preread_code') ? ' has-error' : '' }}">
            <label for="search-subcategory" class="col-sm-2 control-label">Bài đọc trước</label>
            <div class="col-sm-2">
                <input type="text" id="input-preread" name="preread_code" class="form-control" value="{{ $preread_code }}" />
            </div>
            @if ($errors->has('preread_code'))
                <span class="help-block">
                        <strong>{{ $errors->first('preread_code') }}</strong>
                    </span>
            @endif
        </div>
        <div class="form-group">
            <label for="search-subcategory" class="col-sm-2 control-label">Nội dung</label>
            <div class="col-sm-10">
                <textarea name="content" id="" cols="30" rows="10" class="form-control">{{ $lesson->content }}</textarea>
            </div>
        </div>
        <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
            <label for="search-subcategory" class="col-sm-2 control-label">Mã bài học</label>
            <div class="col-sm-2">
                <input type="text" name="code" value="{{ $lesson->code }}" class="form-control" />
            </div>
            @if ($errors->has('code'))
                <span class="help-block">
                    <strong>{{ $errors->first('code') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary btn-register">Đăng ký</button>
                @unless ($creating)
                    <button type="submit" class="btn btn-danger btn-delete">Xóa</button>
                @endunless
                <a href="{{ url('/lesson') }}" class="btn btn-default">Hủy bỏ</a>
            </div>
        </div>
    </form>
@endsection