@extends('layouts.app')

@section('title', 'Welcome')

@section('content')
    <div style="height: 300px;">
        Welcome to Backyard.
        @if (Auth::guest()) Please <a href="{{ url('/login') }}">login</a> to manage lessons. @endif
    </div>
@endsection
