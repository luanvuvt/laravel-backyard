jQuery(document).ready(function ($) {
	// Check Preread
	function checkPreread() {
		var $preread = $('#input-preread');

		if (!$preread.length) {
			return false;
		}

		if (!$preread.data('value')) {
			$preread.data('value', $preread.val());
		}

		$preread.prop('disabled', $('#input-preread-no').prop('checked'));

		if (!$preread.prop('disabled')) {
			$preread.val($preread.data('value'));
		} else {
			$preread.val('');
		}
	}

	$('#input-preread-yes, #input-preread-no').on('click', checkPreread);

	checkPreread();

	// Delete lesson
	$('.form-lesson .btn-delete').on('click', function (evt) {
		if (!confirm('Bạn chắc chắn muốn xóa bài học này?')) {
			return false;
		}

		// Spoof DELETE request
		$('.form-lesson [name="_method"]').val('DELETE');
	});

	// Upload CSV
	if (typeof window.Dropzone != 'undefined') {
		Dropzone.options.dropzone = {
			paramName: 'file',
			acceptedFiles: '.csv',
			previewTemplate: $('.template-result').html(),
			previewsContainer: '.dropzone-previews',
			dictDefaultMessage: 'Drop CSV file here or click to upload.',
			sending: function () {
				$('.form-import').css('display', 'none');
			},
			success: function (file, response) {
				$('.form-import').css('display', 'block');

				$('.form-import input[name="file_name"]').val(response.file_name);
			},
			error: function (file, response) {
				if (typeof response.status == 'undefined') {
					alert('There is a error while uploading this file.');
				} else {
					var message = ['ERROR'];

					if ($.isArray(response.errors)) {
						for (var i in response.errors) {
							message.push(response.errors[i]);
						}
					}

					alert(message.join('\n'));
				}
			}
		};
	}
});