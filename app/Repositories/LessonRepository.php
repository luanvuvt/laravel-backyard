<?php
namespace App\Repositories;

use App\Lesson;

class LessonRepository {
	public function generateCode() {
		$lesson = Lesson::orderBy('id', 'desc')->first();

		if ($lesson) {
			return 'Y' . str_pad(++$lesson->id, 4, '0', STR_PAD_LEFT);
		} else {
			return 'Y0001';
		}
	}

	public function getLessonIdByCode($code) {
		$lesson = Lesson::where('code', $code)->first();

		return $lesson ? $lesson->id : null;
	}
}