<?php
namespace App\Repositories;

use App\Subcategory;

class SubcategoryRepository {
	public function getAllIds() {
		return Subcategory::pluck('id')->toArray();
	}
}