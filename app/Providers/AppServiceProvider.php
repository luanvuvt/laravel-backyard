<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Lesson;
use Validator;
use Illuminate\Http\Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
	    $this->registerCustomValidators($request);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function registerCustomValidators(Request $request) {
	    Validator::extend('lesson_code_unique', function($attribute, $value, $parameters, $validator) use ($request) {
		    if ('code' == $attribute) {
			    $lesson = Lesson::find($request->input('id'));

			    if ($lesson && $lesson->code == $value) { // No change code
				    return true;
			    } else { // Code changed
				    $countExisting = Lesson::where('code', $value)->count();

				    return $countExisting > 0 ? false : true;
			    }
		    } else {
			    return true;
		    }
	    });

	    Validator::extend('preread_code_required', function($attribute, $value, $parameters, $validator) use ($request) {
		    if ('preread' != $attribute || !$value) {
			    return true;
		    } else {
			    return $request->input('preread_code') ? true : false;
		    }
	    });

	    Validator::extend('preread_existing', function($attribute, $value, $parameters, $validator) use ($request) {
		    if ('preread_code' != $attribute) {
			    return true;
		    } else {
			    return Lesson::where('code', $value)->first() ? true : false;
		    }
	    });
    }
}
