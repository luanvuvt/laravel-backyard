<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
	public $fillable = ['code', 'category_id', 'subcategory_id', 'lesson_name_id', 'content', 'preread_id'];

	public function getIsPrereadAttribute($value) {
		return $this->preread_id ? true : false;
	}

	public function category() {
		return $this->belongsTo('App\Category');
	}

	public function subcategory() {
		return $this->belongsTo('App\Subcategory');
	}

	public function lessonName() {
		return $this->belongsTo('App\LessonName');
	}

	public function preread() {
		return $this->belongsTo('App\Lesson', 'preread_id');
	}
}
