<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LessonRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        'category_id'    => 'required',
	        'subcategory_id' => 'required',
	        'lesson_name_id' => 'required',
	        'preread'        => 'preread_code_required',
	        'preread_code'   => 'preread_existing',
	        'code'           => 'required|lesson_code_unique',
        ];
    }

	public function messages()
	{
		return [
			'code.required'                 => 'Mã bài học bắt buộc phải nhập.',
			'code.lesson_code_unique'       => 'Mã bài học đã tồn tại.',
			'preread.preread_code_required' => 'Bài đọc trước bắt buộc phải nhập nếu bạn chọn Đọc trước.',
			'preread_code.preread_existing' => 'Bài đọc trước không tồn tại.',
		];
	}
}
