<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/lesson', 'LessonController@index');
Route::get('/lesson/create', 'LessonController@create');
Route::post('/lesson', 'LessonController@store');
Route::get('/lesson/{lesson}/edit', 'LessonController@edit');
Route::put('/lesson/{lesson}', 'LessonController@update');
Route::delete('/lesson/{lesson}', 'LessonController@destroy');
Route::get('/lesson/import-csv', 'LessonController@import');
Route::post('/lesson/upload-csv', 'LessonController@upload');
Route::post('/lesson/do-import', 'LessonController@doImport');