<?php

namespace App\Http\Controllers;

use App\Category;
use App\Lesson;
use App\LessonName;
use App\Repositories\SubcategoryRepository;
use App\Subcategory;
use App\Repositories\LessonRepository;
use Input;
use Excel;

use Validator;

use App\Http\Requests\LessonRequest;

use Illuminate\Http\Request;

use App\Http\Requests;

class LessonController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
	}

	public function index(Request $request, SubcategoryRepository $subcategoryRepo) {
		$criteria = array(
			'category_id'    => $request->input('category_id', 0),
			'preread'        => $request->input('preread', 1),
		);

		$criteria['subcategory_id'] = $request->input('subcategory_id', $request->exists('category_id') || $request->exists('preread') ?  array() : $subcategoryRepo->getAllIds());

		$lessons = Lesson::with('category', 'subcategory', 'lessonName', 'preread');

		if (0 != $criteria['category_id']) {
			$lessons->where('category_id', $criteria['category_id']);
		}

		if (!empty($criteria['subcategory_id'])) {
			$lessons->where(function ($query) use ($criteria) {
				foreach ($criteria['subcategory_id'] as $subcategory_id) {
					$query->orWhere('subcategory_id', $subcategory_id);
				}
			});
		} else {
			$lessons->where('subcategory_id', 'no_exist');
		}

		if (1 == $criteria['preread']) {
			$lessons->where('preread_id', '>', 0);
		} else {
			$lessons->where('preread_id', null);
		}

		// @todo: Allows to order lessons

		$lessons = $lessons->paginate(5);

		return view('lessons.index', [
			'criteria' => $criteria,
			'lessons' => $lessons,
			'categories' => Category::all(),
			'subcategories' => Subcategory::all(),
		]);
	}

	public function create(Request $request, LessonRepository $lessonRepo) {
		$lesson = new Lesson();
		$lesson->category_id = $request->old('category_id');
		$lesson->subcategory_id = $request->old('subcategory_id');
		$lesson->lesson_name_id = $request->old('lesson_name_id');
		$lesson->content = $request->old('content');
		$lesson->code = $request->old('code', $lessonRepo->generateCode());

		return view('lessons.form', [
			'creating'      => true,
			'is_preread'    => $request->old('preread', 0) ? true : false,
			'preread_code'  => $request->old('preread_code'),
			'categories'    => Category::all(),
			'subcategories' => Subcategory::all(),
			'lesson_names'  => LessonName::all(),
			'lesson'        => $lesson,
		]);
	}

	public function store(LessonRequest $request, LessonRepository $lessonRepo) {
		$data = $request->all();
		$data['preread_id'] = $lessonRepo->getLessonIdByCode($request->input('preread_code'));

		$lesson = Lesson::create($data);

		return redirect('/lesson')
			->with('success', 'Bài học mới đã được tạo.');
	}

	public function edit(Request $request, $lessonId) {
		$lesson = Lesson::with('preread')->findOrFail($lessonId);

		$is_preread = $lesson->is_preread;

		$preread_code = '';
		if ($lesson->preread) {
			$preread_code = $lesson->preread->code;
		}

		if ($request->old('category_id')) {
			$lesson->category_id = $request->old('category_id');
			$lesson->subcategory_id = $request->old('subcategory_id');
			$lesson->lesson_name_id = $request->old('lesson_name_id');
			$lesson->content = $request->old('content');
			$lesson->code = $request->old('code');

			if (!$request->old('preread_code')) {
				$lesson->preread_id = 0;
			}

			$preread_code = $request->old('preread_code');

			$is_preread = $request->old('preread') ? true : false;
		}

		return view('lessons.form', [
			'creating'      => false,
			'is_preread'    => $is_preread,
			'preread_code'  => $preread_code,
			'categories'    => Category::all(),
			'subcategories' => Subcategory::all(),
			'lesson_names'  => LessonName::all(),
			'lesson'        => $lesson,
		]);
	}

	public function update(LessonRequest $request, LessonRepository $lessonRepo, $lessonId) {
		$lesson = Lesson::findOrFail($lessonId);
		$lesson->category_id = $request->input('category_id');
		$lesson->subcategory_id = $request->input('subcategory_id');
		$lesson->lesson_name_id = $request->input('lesson_name_id');
		$lesson->preread_id = $lessonRepo->getLessonIdByCode($request->input('preread_code'));
		$lesson->content = $request->input('content');
		$lesson->code = $request->code;

		$lesson->save();

		return back()
			->with('success', 'Thông tin bài học đã được cập nhật.');
	}

	public function destroy($lessonId) {
		$lesson = Lesson::findOrFail($lessonId);
		$lesson->delete();

		Lesson::where('preread_id', $lessonId)
			->update(
				['preread_id' => null]
			);

		return redirect('/lesson')
			->with('success', 'Bài học đã được xóa.');
	}

	public function import() {
		return view('lessons.import');
	}

	public function upload(Request $request) {
		$file = $request->file('file');

		$validator = Validator::make(
			[
				'file' => $file,
				'extension' => strtolower($file->getClientOriginalExtension()),
			],
			[
				'file' => 'required',
				'extension' => 'required|in:csv',
			]
		);

		if ($validator->fails()) {
			$errors = $validator->messages();
			$response_errors = [];
			if ($errors->first('file')) {
				$response_errors[] = $errors->first('file');
			}
			if ($errors->first('extension')) {
				$response_errors[] = $errors->first('extension');
			}

			return response()->json([
				'status' => 'error',
				'errors' => $response_errors,
			], 400);
		}

		// Prevent from conflict file name
		$fileName = rand(11111, 99999) . '.' . $request->file('file')->getClientOriginalExtension();
		$request->file('file')->move('uploads', $fileName);

		return response()->json([
			'status'    => 'success',
			'file_name' => $fileName,
		]);
	}

	public function doImport(Request $request) {
		$validator = Validator::make(
			$request->all(),
			[
				'file_name' => 'required',
			]
		);

		if ($validator->fails()) {
			return redirect('/lesson/import-csv')
				->withErrors($validator);
		}

		$csvPath = public_path('uploads/' . $request->file_name);

		if (!file_exists($csvPath)) {
			return redirect('/lesson/import-csv')
				->withErrors(['File does not exist.']);
		}

		Excel::load($csvPath, function ($reader) {
			$reader->each(function($sheet) {
				$data = $sheet->toArray();
				if (empty($data['preread_id'])) {
					$data['preread_id'] = null;
				}

				Lesson::create($data);
			});
		});

		// @todo: Remove the uploaded CSV

		return redirect('/lesson')
			->with('success', 'CSV Imported');
	}
}
